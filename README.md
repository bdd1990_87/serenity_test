# Details on changes done

- Correct gradle.build file to resolve build failures.
- corrected step definition class to resolve test failure for the initial code shared with me.
- Step defition refactoring
    1. Removed @Steps annonation for CarsApi object since its unsused
    2. Corrected assertions for mango and made it generic to verify title contains the value of product(can be any valid product) which we are trying to get using get API.
    3. Corrected assertion for validating invalid product retruns error attribute in response.
    4. Used the base url and endpoint value as before setup and removed the API url from feature file
- Removed CarsApi class as its unused.
- Added baseURL and endpoint to serenity config
- Removed browser related configs from serenity properties since its not needed
- Refactored feature file 2 have generic scenario for positive and negative case
    1. Positive case : to validate status code when valid product is provided as an input to get product endpoint and verify that in list of results each objects title contains product name in its value (covers 4 values apple, mango, tofu and water as inputs)
    2. Negative case : to validate status code is 404 when invlaid product is passed and result should contain error attribute in response. (covers scneario with valid product but case difference in name provided as an input to api ex. apple passed as AppLe in the input, cars)
    3. covered total 9 examples using above two scenarios
    4. Added scenario outline title else serenity doesnt generate reports. the initial feature file passed for test had no ttile to scenarios so no output report was generated.
- Refactored step def to accomodate refactoring of feature file as mentioned in above step.


Few additional things that I would do :-
As of now since we had just one endpoint, I dint do it.
But if we would have multiple endpoints would have 
    - added client classes (these class would have method to call the endpoint, methods to get each attribute of resposne, assert each attribute if response, status code validation for the endpoint) for each in src/main
    - added model clasee for request and response of endpoint have getters and setters of the request attribtes and response attributes using lombok.
    - asserthelp class to have all the common assertions like status code or some common error thrown by all endpoints so that there is no duplication.

