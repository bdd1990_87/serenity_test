package starter.stepdefinitions;

import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.util.EnvironmentVariables;

import static net.serenitybdd.rest.SerenityRest.restAssuredThat;

public class SearchStepDefinitions {

	private EnvironmentVariables environmentVariables;
	private String baseUrl;
	private String getProductsEndpoint;

	@Before
	public void configureBaseUrl() {
		baseUrl = environmentVariables.optionalProperty("restapi.baseurl")
				.orElse("https://waarkoop-server.herokuapp.com/api/");
		getProductsEndpoint = environmentVariables.optionalProperty("resptapi.getproducts").orElse("v1/search/test/");
	}

	@When("he calls get products endpoint for {string}")
	public void heCallsGetProductsEndpoint(String product) {
		SerenityRest.given().get(baseUrl + getProductsEndpoint + product);
	}

	@Then("he sees the results displayed")
	public void heSeesTheResultsDisplayed() {
		restAssuredThat(response -> response.statusCode(200));
	}

	@Then("he sees the results title contains {string} for all the listed products")
	public void heSeesTheResultsTitlecontainsProductName(String product) {
		restAssuredThat(response -> response.assertThat().extract().jsonPath().getList("title").contains(product));
	}

	@Then("he doesn not see the results")
	public void heDoesnNotSeeTheResults() {
		restAssuredThat(response -> response.statusCode(404));
	}

	@And("the results contain error")
	public void theResultsContainsError() {
		restAssuredThat(
				response -> response.assertThat().extract().jsonPath().getJsonObject("detail.error").equals(true));
	}
}
