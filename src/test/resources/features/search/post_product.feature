Feature: Search for the product

### Please use endpoint GET https://waarkoop-server.herokuapp.com/api/v1/search/test/{product} for getting the products.
### Available products: "apple", "mango", "tofu", "water"
### Prepare Positive and negative scenarios

@Sanity
Scenario Outline: Verify get products when a valid product is provided in get endpoint
    When he calls get products endpoint for <product>
    Then he sees the results displayed
    And he sees the results title contains <product> for all the listed products
Examples:    
    |product|
    |"apple"|
    |"mango"|
    |"tofu" |
    |"water"|
   
 Scenario Outline: Verify get products when an invalid product is provided in get endpoint
   When he calls get products endpoint for <invalidProduct>
   Then he doesn not see the results
   And the results contain error
Examples:    
    |invalidProduct|
    |    "car"     |
    |    "APPLE"   |
    |    "mangO"   |
    |	 "tOFu"	   |
    |    "wat1er"  |
  